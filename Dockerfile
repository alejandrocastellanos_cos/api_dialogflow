# Dockerfile
FROM python:3.6.8
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["index.py"]

# docker build -t img_flaskinfobip:latest .
# docker run -d -p 8084:5003 --name flaskinfobip img_flaskinfobip:latest