from flask import Flask, request, jsonify, render_template
import os
import dialogflow
import requests
import json
from datetime import date

app = Flask(__name__)

@app.route('/api_dialogflow')
def index():
    numero = '3115730380'
    hoy = str(date.today())
    fichero = numero + hoy
    return ('La hora es {}'.format(fichero))


def detect_intent_texts(project_id, session_id, text, language_code):
    session_client = dialogflow.SessionsClient()
    session = session_client.session_path(project_id, session_id)

    if text:
        text_input = dialogflow.types.TextInput(
            text=text, language_code=language_code)
        query_input = dialogflow.types.QueryInput(text=text_input)
        response = session_client.detect_intent(
            session=session, query_input=query_input)
        print(response.query_result.fulfillment_text)
        return response.query_result.fulfillment_text


@app.route('/api_dialogflow/send_message', methods=['POST'])
def send_message():
    message = request.json['message']
    print(message)
    project_id = os.getenv('DIALOGFLOW_PROJECT_ID')
    fulfillment_text = detect_intent_texts(project_id, "unique", message, 'en')
    response_text = { "message":  fulfillment_text }
    return jsonify(response_text)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port='5000')